#! /bin/bash

files_str=$(git diff --name-only origin/master $BITBUCKET_BRANCH | grep $1)
readarray -t files_array <<<"$files_str"
for i in "${files_array[@]}"; do
  printf "${i##*/},"
done
